public class LinkedList {

    Node head; // Head of the list

    // Inner class for individual nodes
    static class Node {
        Object data;
        Node next;
        Node(Object data) {
            this.data = data;
        }
    }

    // Add a new node at the end
    public void add(Object data) {
        Node newNode = new Node(data);

        if (head == null) {
            head = newNode;
        } else {
            Node current = head;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        }
    }

    // Remove the first occurrence of an element
    public void remove(Object data) {
        if (head == null) return;

        if (head.data.equals(data)) {
            head = head.next;
            return;
        }

        Node current = head;
        while (current.next != null) {
            if (current.next.data.equals(data)) {
                current.next = current.next.next;
                return;
            }
            current = current.next;
        }
    }

    // Check if an element exists
    public boolean contains(Object data) {
        Node current = head;
        while (current != null) {
            if (current.data.equals(data)) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    // Remove all elements
    public void clear() {
        head = null;
    }

    // Check if empty
    public boolean isEmpty() {
        return head == null;
    }

    // Update element at a given index
    public void set(int index, Object data) {
        if (index < 0) {
            throw new IndexOutOfBoundsException();
        }

        Node current = head;
        int count = 0;
        while (current != null) {
            if (count == index) {
                current.data = data;
                return;
            }
            count++;
            current = current.next;
        }

        throw new IndexOutOfBoundsException();
    }

    // Get the number of elements
    public int size() {
        int count = 0;
        Node current = head;
        while (current != null) {
            count++;
            current = current.next;
        }
        return count;
    }

    // String representation for printing
    @Override
    public String toString() {
        if (head == null) return "[]";

        StringBuilder sb = new StringBuilder("[");
        Node current = head;
        while (current.next != null) {
            sb.append(current.data).append(", ");
            current = current.next;
        }
        sb.append(current.data).append("]");
        return sb.toString();
    }
}